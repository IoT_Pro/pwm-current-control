const float max_current_limit = 2.0;
const int pwm_pin = 9;
int pwm = 0;

void setup() {
  Serial.begin(9600);
  pinMode(pwm_pin, OUTPUT);
  analogWrite(pwm_pin, 0);
}

void loop() {
  analogWrite(pwm_pin, pwm);

  float I = 0;
  for (int i = 0; i < 1000; i++) {
    I = I + (0.074 * analogRead(A5) - 37.87) / 1000;
  }
  I = I / 1.22;

  if (I <= max_current_limit) {
    pwm++;
  }
  if (I > max_current_limit) {
    pwm--;
  }
  delay(200);
}