const float max_current_limit = 5.0;
const int mosfet_pin_1 = 9;
const int mosfet_pin_2 = 10;
const int relay_pin = 2;
const long period_time = 20000;  // 300000 milisecond = 300s = 5min
int pwm_1 = 0;
int pwm_2 = 0;

bool flag_1 = true;
bool flag_2 = true;

void setup() {
  Serial.begin(9600);
  pinMode(mosfet_pin_1, OUTPUT);
  pinMode(mosfet_pin_2, OUTPUT);
}

void loop_1() {
  while (flag_1 == true) {
    analogWrite(mosfet_pin_1, pwm_1);
    float I = 0;
    for (int i = 0; i < 1000; i++) {
      I = I + (analogRead(A1) - 400.0) * 5.0 / 1023.0 / 0.066 / 1000;  // C1 = 0.074, C2 = 37.87
    }
    if (I >= 8 - max_current_limit) {
      pwm_1 = min(pwm_1 + 5, 255);
    } else {
      flag_1 = false;
    }
  }
}

void loop_2() {
  while (flag_2 == true) {
    analogWrite(mosfet_pin_2, pwm_2);
    float I = 0;
    for (int i = 0; i < 1000; i++) {
      I = I + (analogRead(A1) - 400.0) * 5.0 / 1023.0 / 0.065 / 1000;  // C1 = 0.074, C2 = 37.87
    }
    if (I >= 7.6 - max_current_limit) {
      pwm_2 = min(pwm_2 + 5, 255);
    } else {
      flag_2 = false;
    }
  }
}

void loop() {
  loop_1();
  delay(period_time);
  analogWrite(mosfet_pin_1, 0);
  delay(3000);
  
  loop_2();
  delay(period_time);
  analogWrite(mosfet_pin_2, 0);
  delay(3000);

  pwm_1 = 0;
  pwm_2 = 0;
  flag_1 = true;
  flag_2 = true;
}
